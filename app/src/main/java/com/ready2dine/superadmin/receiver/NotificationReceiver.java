package com.ready2dine.superadmin.receiver;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.services.BackgroundSoundService;
import com.ready2dine.superadmin.util.AppUtils;
import com.ready2dine.superadmin.util.Config;

/**
 * Created by DEBASHISH on 8/31/2018.
 */

    public class NotificationReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        playNotificationSound(context);
    }

    public void playNotificationSound(final Context context) {
        try {
            /*Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + context.getPackageName() + "/raw/ready2dine_order");
            Ringtone r = RingtoneManager.getRingtone(context, alarmSound);
            r.play();*/

            SharedPreferences pref = context.getSharedPreferences(Config.SHARED_PREF, 0);
            boolean value = pref.getBoolean("playing", false);

            if (!value) {
                AppUtils.setPlaying(context, true);

                Intent myService = new Intent(context, BackgroundSoundService.class);
                context.startService(myService);

               // MediaPlayer mp = MediaPlayer.create(context, R.raw.tone1);
               // mp.start();

//                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                    @Override
//                    public void onCompletion(MediaPlayer mediaPlayer) {
//                        AppUtils.setPlaying(context, false);
//                    }
//                });

            }


            Intent i = new Intent("MY_BROADCAST").putExtra("something", "isnothing");
            context.sendBroadcast(i);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}