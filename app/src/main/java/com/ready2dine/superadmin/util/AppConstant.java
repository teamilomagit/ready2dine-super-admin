package com.ready2dine.superadmin.util;

/**
 * Created by DEBASHISH on 8/20/2018.
 */

public class AppConstant {


    //LIVE SERVER
    public static final String BASE_URL = "http://ready2dine.co.in/Projects/ready2dine_live/mobile_api/";


    //STAGING SERVER
  //  public static final String BASE_URL = "http://stagingr2d.esy.es/mobile_api/";



    public static final String GET_CURRENT_ORDERS = BASE_URL + "order/getCurrentOrders";
    public static final String GET_IN_PROCESS_ORDERS = BASE_URL + "order/getInProcessOrders";
    public static final String API_ORDER_DETAIL = BASE_URL + "/order/get_order_detail";
    public static final String CHANGE_ORDER_STATUS = BASE_URL + "order/changeOrderStatus";
    public static final String API_SAVE_PUSH_TOKEN = BASE_URL + "/user/save_push_token";
    public static final String LOGIN = BASE_URL + "user/owner_login";
    public static final String API_LOGOUT = BASE_URL + "/user/logout";
    public static final String API_GET_ORDER_SUPER = BASE_URL + "order/getAllOrdersForSuperAdmin";
    public static final String API_LOGIN_SUPER = BASE_URL + "user/super_admin_login";
    public static final String API_GET_CUSTOMERS = BASE_URL + "user/get_todays_signup";
    public static final String API_POST_IMAGE = BASE_URL + "master/sendMsgTopic";
    public static final String API_GET_PROFILE_FROM_NUMBER = BASE_URL + "user/get_user_profile_by_mobile";
    public static final String API_MY_SAVINGS_LIST = BASE_URL + "user/get_my_savings";
    public static final String API_MODIFY_WALLET = BASE_URL + "user/add_wallet_amount";
}
