package com.ready2dine.superadmin.util;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.widget.ImageView;

import com.ready2dine.superadmin.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by DEBASHISH on 4/13/2018.
 */

public class PhotoSelector {

    private Uri filePath, imageUri;
    private ImageView imgSelectPic;
    private Bitmap profilePic;
    private Context context;

    static String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    public static final int PERMISSION_ALL = 1;
    public static int REQUEST_CAMERA = 90, SELECT_FILE = 91;


    public PhotoSelector(Context context)
    {
        this.context = context;
    }

    public void selectImage(ImageView img) {
        imgSelectPic = img;
        final CharSequence[] items = {context.getString(R.string.take_photo), context.getString(R.string.choose_from_gallery), context.getString(R.string.btn_cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals(context.getString(R.string.take_photo))) {
                    cameraIntent();
                } else if (items[item].equals(context.getString(R.string.choose_from_gallery))) {
                    galleryIntent();
                } else if (items[item].equals(context.getString(R.string.btn_cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void cameraIntent() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        imageUri = context.getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        ((Activity) context).startActivityForResult(intent, REQUEST_CAMERA);
    }

    public void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        ((Activity) context).startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    @SuppressWarnings("deprecation")
    public Uri onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(context.getContentResolver(), data.getData());
                imgSelectPic.setImageBitmap(bm);

                return filePath = data.getData();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public Uri onCaptureImageResult() {
        Bitmap photo = null;
        try {
            photo = MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
        SaveImage(getResizedBitmap(photo, 800));
        imgSelectPic.setImageBitmap(getResizedBitmap(photo, 800));
        return filePath = getImageUri(profilePic);
    }

    public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        profilePic = Bitmap.createScaledBitmap(image, width, height, true);
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void SaveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/R2D");

        if (!myDir.exists()) {
            myDir.mkdirs();
            myDir = new File(root + "/R2D/Images");
            if (!myDir.exists()) {
                myDir.mkdirs();
            }
        } else {
            myDir = new File(root + "/R2D/Images");
            if (!myDir.exists()) {
                myDir.mkdirs();
            }
        }
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "imgR2D" + n + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            filePath = getImageUri(finalBitmap);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getPath(Uri uri, final Context context) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = context.getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();
        return path;
    }

    /**
     * PERMISSION SECTION
     **/

    public boolean isPermissionGranted(Context context) {

        if (!hasPermissions(context, PERMISSIONS)) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.CAMERA)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

//                showPermissionDisclaimerDialog(context);
                ActivityCompat.requestPermissions((Activity) context, PERMISSIONS, PERMISSION_ALL);

                return false;
            } else if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

//                showPermissionDisclaimerDialog(context);
                ActivityCompat.requestPermissions((Activity) context, PERMISSIONS, PERMISSION_ALL);

                return false;
            } else {

                // No explanation needed; request the permission
                ActivityCompat.requestPermissions((Activity) context, PERMISSIONS, PERMISSION_ALL);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
                return false;
            }
        } else {
            // Permission has already been granted
            return true;
        }
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


}
