package com.ready2dine.superadmin.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by DEBASHISH on 3/29/2018.
 */

public class AppUtils {

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public final static boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    public static String convertDate(Context context, String formerDate) {

        String result = "";

        try {
            Date convertedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(formerDate);
            result = new SimpleDateFormat("dd MMM hh:mm a").format(convertedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        }

        return result.toUpperCase();
    }


    public static void setPlaying(Context context, boolean value) {
        SharedPreferences pref = context.getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("playing", value);
        editor.commit();
    }

}
