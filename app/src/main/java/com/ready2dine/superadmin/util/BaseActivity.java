package com.ready2dine.superadmin.util;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ready2dine.superadmin.R;

/**
 * Created by DEBASHISH on 3/30/2018.
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    /**
     * Custom Layout Functionality
     **/
    protected void setCustomToolBarWithTitle(String title) {
        TextView txtTitle = findViewById(R.id.txt_title);
        txtTitle.setText(title);
        setCustomToolBarBackPress();
    }

    protected void setCustomToolBarBackPress() {
        ImageView imgBack = findViewById(R.id.img_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
