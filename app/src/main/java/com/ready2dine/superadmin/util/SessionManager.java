package com.ready2dine.superadmin.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ready2dine.superadmin.model.UserModel;

/**
 * Created by Debashish on 3/19/2018.
 */

public class SessionManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "AndroidHivePref";
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_USER = "user";

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(UserModel userModel) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(userModel);
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USER, jsonString);
        editor.commit();
    }


    public UserModel getUserModel() {
        UserModel userModel = null;
        Gson gson = new Gson();
        String jsonString = pref.getString(KEY_USER, "");
        userModel = gson.fromJson(jsonString, UserModel.class);
        return userModel;
    }

    public void updateLoginSession(UserModel userModel) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(userModel);
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USER, jsonString);
        editor.commit();
    }


    /**
     * Clear session details
     */
    public void logoutUser() {
        Gson gson = new Gson();
        String jsonString = gson.toJson(null);
        editor.putBoolean(IS_LOGIN, false);
        editor.putString(KEY_USER, jsonString);
        editor.commit();
    }


    public void clear() {
        editor.putString(KEY_USER, null);
        editor.commit();
    }
}
