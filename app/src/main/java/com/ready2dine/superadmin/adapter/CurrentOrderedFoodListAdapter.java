package com.ready2dine.superadmin.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.activity.CompleteOrderActivity;
import com.ready2dine.superadmin.activity.CurrentOrderDetailActivity;
import com.ready2dine.superadmin.model.OrderItemModel;

import java.util.ArrayList;

/**
 * Created by Debashish on 7/19/2017.
 */

public class CurrentOrderedFoodListAdapter extends RecyclerView.Adapter<CurrentOrderedFoodListAdapter.ViewHolder> {
    Context context;
    ArrayList<OrderItemModel> list;

    public CurrentOrderedFoodListAdapter(Context context, ArrayList<OrderItemModel> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtOrderedFoodName, txtOrderedTotalPrice, txtOrderedFoodQty, txtOrderedFoodPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            txtOrderedFoodName = itemView.findViewById(R.id.txt_ordered_food);
            txtOrderedFoodQty = itemView.findViewById(R.id.txt_ordered_food_qty);
            txtOrderedFoodPrice = itemView.findViewById(R.id.txt_ordered_food_price);
            txtOrderedTotalPrice = itemView.findViewById(R.id.txt_total_price);
        }
    }

    public void updateAdapter(ArrayList<OrderItemModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_item_current_food_order, parent, false);
        return new CurrentOrderedFoodListAdapter.ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        OrderItemModel model = list.get(position);

        float totalVal = Float.parseFloat(model.getPrice()) * model.getQuantity();

        holder.txtOrderedFoodName.setText(model.getItem_name());
        holder.txtOrderedFoodQty.setText(model.getQuantity() + "");
        holder.txtOrderedFoodPrice.setText(context.getString(R.string.rs_symbol) + " " + model.getPrice());
        holder.txtOrderedTotalPrice.setText(context.getString(R.string.rs_symbol) + " " + totalVal);

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


}
