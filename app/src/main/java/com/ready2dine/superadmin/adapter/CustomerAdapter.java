package com.ready2dine.superadmin.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.model.CustomerModel;
import com.ready2dine.superadmin.model.RewardModel;

import java.util.ArrayList;

/**
 * Created by Debashish on 7/19/2017.
 */

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.ViewHolder> {
    Context context;
    ArrayList<CustomerModel> list;


    public CustomerAdapter(Context context, ArrayList<CustomerModel> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtCustomerName;
        TextView txtCustomerNumber;

        public ViewHolder(View itemView) {
            super(itemView);
            txtCustomerName = itemView.findViewById(R.id.txt_customer_name);
            txtCustomerNumber = itemView.findViewById(R.id.txt_customer_number);
        }
    }

    public void updateAdapter(ArrayList<CustomerModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_item_customer, parent, false);
        return new CustomerAdapter.ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final CustomerModel model = list.get(position);

        holder.txtCustomerName.setText(model.getDisplay_name());
        holder.txtCustomerNumber.setText(model.getMobile());

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


}
