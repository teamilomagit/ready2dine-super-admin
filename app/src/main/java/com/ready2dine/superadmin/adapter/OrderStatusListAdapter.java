package com.ready2dine.superadmin.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.activity.CompleteOrderActivity;
import com.ready2dine.superadmin.model.OrderModel;
import com.ready2dine.superadmin.util.AppUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Debashish on 7/19/2017.
 */

public class OrderStatusListAdapter extends RecyclerView.Adapter<OrderStatusListAdapter.ViewHolder> {
    Context context;
    ArrayList<OrderModel> list;


    public OrderStatusListAdapter(Context context, ArrayList<OrderModel> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtCustomerName;
        TextView txtOrderStatus;
        TextView txtOrderTimeGuests;
        TextView txtRestaurantName;
        TextView txtRestaurantPhone;
        ImageView imgStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            txtCustomerName = itemView.findViewById(R.id.txt_customer_name);
            txtOrderStatus = itemView.findViewById(R.id.txt_status);
            txtOrderTimeGuests = itemView.findViewById(R.id.txt_time_guests);
            txtRestaurantName = itemView.findViewById(R.id.txt_restaurant_name);
            txtRestaurantPhone = itemView.findViewById(R.id.txt_restaurant_phone);
            imgStatus = itemView.findViewById(R.id.img_status);
        }
    }

    public void updateAdapter(ArrayList<OrderModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_item_in_process, parent, false);
        return new OrderStatusListAdapter.ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final OrderModel model = list.get(position);

        holder.txtCustomerName.setText(model.getCustomer_name());
        holder.txtRestaurantName.setText(model.getRestaurant_name());
        holder.txtRestaurantPhone.setText(model.getRestaurant_mobile());


        if (model.getNumber_of_guest() > 1) {
            holder.txtOrderTimeGuests.setText(AppUtils.convertDate(context, model.getBooking_date()) + " for " + model.getNumber_of_guest() + " Guests");
        } else {
            holder.txtOrderTimeGuests.setText(AppUtils.convertDate(context,model.getBooking_date()) + " for " + model.getNumber_of_guest() + " Guest");
        }

        if (model.getStatus().equalsIgnoreCase(context.getString(R.string.order_accepted))) {
            holder.txtOrderStatus.setText(context.getString(R.string.order_in_process));
            holder.txtOrderStatus.setTextColor(ContextCompat.getColor(context, R.color.colorGreen));
            Picasso.with(context).load(R.mipmap.ic_order_reload).into(holder.imgStatus);
        } else if (model.getStatus().equalsIgnoreCase(context.getString(R.string.order_cancelled))) {
            holder.txtOrderStatus.setText(context.getString(R.string.order_cancelled));
            holder.txtOrderStatus.setTextColor(ContextCompat.getColor(context, R.color.colorRed));
            Picasso.with(context).load(R.mipmap.ic_order_cancel).into(holder.imgStatus);
        } else if (model.getStatus().equalsIgnoreCase(context.getString(R.string.order_declined))) {
            holder.txtOrderStatus.setText(context.getString(R.string.order_declined));
            holder.txtOrderStatus.setTextColor(ContextCompat.getColor(context, R.color.colorRed));
            Picasso.with(context).load(R.mipmap.ic_order_declined).into(holder.imgStatus);
        } else if (model.getStatus().equalsIgnoreCase(context.getString(R.string.order_completed))) {
            holder.txtOrderStatus.setText(context.getString(R.string.order_completed));
            holder.txtOrderStatus.setTextColor(ContextCompat.getColor(context, R.color.colorBlue));
            Picasso.with(context).load(R.mipmap.ic_order_complete).into(holder.imgStatus);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, CompleteOrderActivity.class).putExtra("process_order_model", new Gson().toJson(model)));
            }
        });

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


}
