package com.ready2dine.superadmin.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.activity.CurrentOrderDetailActivity;
import com.ready2dine.superadmin.model.OrderModel;
import com.ready2dine.superadmin.util.AppUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Debashish on 7/19/2017.
 */

public class CurrentOrderListAdapter extends RecyclerView.Adapter<CurrentOrderListAdapter.ViewHolder> {
    Context context;
    ArrayList<OrderModel> list;

    public CurrentOrderListAdapter(Context context, ArrayList<OrderModel> list) {
        this.context = context;
        this.list = list;
    }

    public void updateAdapter(ArrayList<OrderModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtCustomerName, txtGuestsAndTime, txtCustomerBill, txtRestaurantName, txtRestaurantPhone;

        public ViewHolder(View itemView) {
            super(itemView);
            txtCustomerName = itemView.findViewById(R.id.txt_customer_name);
            txtGuestsAndTime = itemView.findViewById(R.id.txt_time_guests);
            txtCustomerBill = itemView.findViewById(R.id.txt_customer_bill);
            txtRestaurantName = itemView.findViewById(R.id.txt_restaurant_name);
            txtRestaurantPhone = itemView.findViewById(R.id.txt_restaurant_phone);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_item_current_orders, parent, false);
        return new CurrentOrderListAdapter.ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final OrderModel model = list.get(position);

        holder.txtCustomerName.setText(model.getCustomer_name());
        holder.txtCustomerBill.setText("₹ " + new DecimalFormat("##.##").format(calculateBill(model)));
        holder.txtRestaurantName.setText(model.getRestaurant_name());
        holder.txtRestaurantPhone.setText(model.getRestaurant_mobile());

        if (model.getNumber_of_guest() > 1) {
            holder.txtGuestsAndTime.setText(AppUtils.convertDate(context, model.getBooking_date()) + " for " + model.getNumber_of_guest() + " Guests");
        } else {
            holder.txtGuestsAndTime.setText(AppUtils.convertDate(context, model.getBooking_date()) + " for " + model.getNumber_of_guest() + " Guest");
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, CurrentOrderDetailActivity.class)
                        .putExtra("current_order_model", new Gson().toJson(model)));
            }
        });

    }


    //Calculate grand total with respect to gst and discount
    private float calculateBill(OrderModel orderModel) {
        float resDiscount = 0;
        float subTotal = orderModel.getSubtotal();
        float gstValue = subTotal * (orderModel.getTax_gst_percent() / 100);


        if (orderModel.getRestro_offer_id() != null) {
            resDiscount = subTotal * (orderModel.getRestro_offer_discount() / 100);
        } else if (orderModel.getRestaurant_discount() > 0) {
            resDiscount = subTotal * (orderModel.getRestaurant_discount() / 100);
        }

        float grandTotal = subTotal + gstValue - resDiscount;
        return grandTotal;
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


}
