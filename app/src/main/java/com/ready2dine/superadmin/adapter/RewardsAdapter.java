package com.ready2dine.superadmin.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.model.RewardModel;

import java.util.ArrayList;

/**
 * Created by Debashish on 7/19/2017.
 */

public class RewardsAdapter extends RecyclerView.Adapter<RewardsAdapter.ViewHolder> {
    Context context;
    ArrayList<RewardModel> list;


    public RewardsAdapter(Context context, ArrayList<RewardModel> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtAmount;
        TextView txtType;

        public ViewHolder(View itemView) {
            super(itemView);
            txtAmount = itemView.findViewById(R.id.txt_amount);
            txtType = itemView.findViewById(R.id.txt_type);
        }
    }

    public void updateAdapter(ArrayList<RewardModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_item_wallet, parent, false);
        return new RewardsAdapter.ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final RewardModel model = list.get(position);

        holder.txtAmount.setText(context.getString(R.string.rs_symbol) + " " + model.getAmount());
        holder.txtType.setText(model.getType());

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


}
