package com.ready2dine.superadmin.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.util.AppUtils;

public class BackgroundSoundService extends Service {

    private static final String TAG = "BackgroundSoundService";
    MediaPlayer player;

    Context context;

    public IBinder onBind(Intent arg0) {
        Log.i(TAG, "onBind()");
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        this.context = this;

        player = MediaPlayer.create(this, R.raw.order_tone);
        player.setLooping(false); // Set looping
        player.setVolume(100, 100);

        Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= 26) {
            vibrator.vibrate(VibrationEffect.createOneShot(2000, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrator.vibrate(2000);
        }

        // Toast.makeText(this, "Service started...", Toast.LENGTH_SHORT).show();
        // Log.i(TAG, "onCreate() , service started...");
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                AppUtils.setPlaying(context, false);
                stopSelf();
            }
        });

    }


    public int onStartCommand(Intent intent, int flags, int startId) {
        player.start();
        return Service.START_STICKY;
    }

    public IBinder onUnBind(Intent arg0) {
        Log.i(TAG, "onUnBind()");
        return null;
    }

    public void onStop() {
        Log.i(TAG, "onStop()");
    }

    public void onPause() {
        Log.i(TAG, "onPause()");
    }

    @Override
    public void onDestroy() {
        player.stop();
        player.release();
//        Toast.makeText(this, "Service stopped...", Toast.LENGTH_SHORT).show();
        Log.i(TAG, "onCreate() , service stopped...");
    }

    @Override
    public void onLowMemory() {
        Log.i(TAG, "onLowMemory()");
    }

}