package com.ready2dine.superadmin.model;

/**
 * Created by DEBASHISH on 8/20/2018.
 */

public class OrderModel {

    private String user_cashback_amount;

    private String booking_date;

    private String user_cashback_percent;

    private String id;

    private String ready2dine_offer_id;

    private String created_at;

    private String service_charge_on_restro;

    private String grand_total;

    private String restaurant_mobile;

    private float restaurant_discount;

    private String restro_offer_id;

    private String status;

    private String file_path;

    private String customer_name;

    private String restaurant_id;

    private String min_order_amount;

    private String ready2dine_up_to_amount;

    private int number_of_guest;

    private float subtotal;

    private String discount;

    private String ready2dine_discount;

    private String special_request;

    private String my_saving_used_amt;

    private String updated_at;

    private String service_charge_on_offer;

    private float restro_offer_discount;

    private String ready2dine_discount_amt;

    private boolean customer_near_restro;

    private String is_r2d_received_payment;

    private float tax_gst_percent;

    private String restaurant_name;

    private String customer_id;

    private String mobile;

    public String getUser_cashback_amount() {
        return user_cashback_amount;
    }

    public void setUser_cashback_amount(String user_cashback_amount) {
        this.user_cashback_amount = user_cashback_amount;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getUser_cashback_percent() {
        return user_cashback_percent;
    }

    public void setUser_cashback_percent(String user_cashback_percent) {
        this.user_cashback_percent = user_cashback_percent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReady2dine_offer_id() {
        return ready2dine_offer_id;
    }

    public void setReady2dine_offer_id(String ready2dine_offer_id) {
        this.ready2dine_offer_id = ready2dine_offer_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getService_charge_on_restro() {
        return service_charge_on_restro;
    }

    public void setService_charge_on_restro(String service_charge_on_restro) {
        this.service_charge_on_restro = service_charge_on_restro;
    }

    public String getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(String grand_total) {
        this.grand_total = grand_total;
    }

    public String getRestaurant_mobile() {
        return restaurant_mobile;
    }

    public void setRestaurant_mobile(String restaurant_mobile) {
        this.restaurant_mobile = restaurant_mobile;
    }

    public float getRestaurant_discount() {
        return restaurant_discount;
    }

    public void setRestaurant_discount(float restaurant_discount) {
        this.restaurant_discount = restaurant_discount;
    }

    public String getRestro_offer_id() {
        return restro_offer_id;
    }

    public void setRestro_offer_id(String restro_offer_id) {
        this.restro_offer_id = restro_offer_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(String restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getMin_order_amount() {
        return min_order_amount;
    }

    public void setMin_order_amount(String min_order_amount) {
        this.min_order_amount = min_order_amount;
    }

    public String getReady2dine_up_to_amount() {
        return ready2dine_up_to_amount;
    }

    public void setReady2dine_up_to_amount(String ready2dine_up_to_amount) {
        this.ready2dine_up_to_amount = ready2dine_up_to_amount;
    }

    public int getNumber_of_guest() {
        return number_of_guest;
    }

    public void setNumber_of_guest(int number_of_guest) {
        this.number_of_guest = number_of_guest;
    }

    public float getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(float subtotal) {
        this.subtotal = subtotal;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getReady2dine_discount() {
        return ready2dine_discount;
    }

    public void setReady2dine_discount(String ready2dine_discount) {
        this.ready2dine_discount = ready2dine_discount;
    }

    public String getSpecial_request() {
        return special_request;
    }

    public void setSpecial_request(String special_request) {
        this.special_request = special_request;
    }

    public String getMy_saving_used_amt() {
        return my_saving_used_amt;
    }

    public void setMy_saving_used_amt(String my_saving_used_amt) {
        this.my_saving_used_amt = my_saving_used_amt;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getService_charge_on_offer() {
        return service_charge_on_offer;
    }

    public void setService_charge_on_offer(String service_charge_on_offer) {
        this.service_charge_on_offer = service_charge_on_offer;
    }

    public float getRestro_offer_discount() {
        return restro_offer_discount;
    }

    public void setRestro_offer_discount(float restro_offer_discount) {
        this.restro_offer_discount = restro_offer_discount;
    }

    public String getReady2dine_discount_amt() {
        return ready2dine_discount_amt;
    }

    public void setReady2dine_discount_amt(String ready2dine_discount_amt) {
        this.ready2dine_discount_amt = ready2dine_discount_amt;
    }

    public boolean getCustomer_near_restro() {
        return customer_near_restro;
    }

    public void setCustomer_near_restro(boolean customer_near_restro) {
        this.customer_near_restro = customer_near_restro;
    }

    public String getIs_r2d_received_payment() {
        return is_r2d_received_payment;
    }

    public void setIs_r2d_received_payment(String is_r2d_received_payment) {
        this.is_r2d_received_payment = is_r2d_received_payment;
    }

    public float getTax_gst_percent() {
        return tax_gst_percent;
    }

    public void setTax_gst_percent(float tax_gst_percent) {
        this.tax_gst_percent = tax_gst_percent;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
