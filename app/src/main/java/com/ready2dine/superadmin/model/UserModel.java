package com.ready2dine.superadmin.model;

/**
 * Created by DEBASHISH on 8/20/2018.
 */

public class UserModel {

    private boolean is_noon_time;

    private boolean is_delete;

    private String gst_no;

    private String password;

    private String cuisines_ids;

    private String city_id;

    private String id;

    private String landline;

    private String price_range;

    private String name;

    private String morning_open_time;

    private String created_at;

    private String longitude;

    private String noon_close_time;

    private String closing_days;

    private String website;

    private String status;

    private boolean is_closed;

    private String file_path;

    private float min_order_amount;

    private float discount;

    private String morning_close_time;

    private String noon_open_time;

    private String updated_at;

    private String address;

    private String email;

    private String video_path;

    private String latitude;

    private boolean is_pure_veg;

    private String mobile;

    private String full_name;


    public boolean isIs_noon_time() {
        return is_noon_time;
    }

    public void setIs_noon_time(boolean is_noon_time) {
        this.is_noon_time = is_noon_time;
    }

    public boolean isIs_delete() {
        return is_delete;
    }

    public void setIs_delete(boolean is_delete) {
        this.is_delete = is_delete;
    }

    public String getGst_no() {
        return gst_no;
    }

    public void setGst_no(String gst_no) {
        this.gst_no = gst_no;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCuisines_ids() {
        return cuisines_ids;
    }

    public void setCuisines_ids(String cuisines_ids) {
        this.cuisines_ids = cuisines_ids;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getPrice_range() {
        return price_range;
    }

    public void setPrice_range(String price_range) {
        this.price_range = price_range;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMorning_open_time() {
        return morning_open_time;
    }

    public void setMorning_open_time(String morning_open_time) {
        this.morning_open_time = morning_open_time;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getNoon_close_time() {
        return noon_close_time;
    }

    public void setNoon_close_time(String noon_close_time) {
        this.noon_close_time = noon_close_time;
    }

    public String getClosing_days() {
        return closing_days;
    }

    public void setClosing_days(String closing_days) {
        this.closing_days = closing_days;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isIs_closed() {
        return is_closed;
    }

    public void setIs_closed(boolean is_closed) {
        this.is_closed = is_closed;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public float getMin_order_amount() {
        return min_order_amount;
    }

    public void setMin_order_amount(float min_order_amount) {
        this.min_order_amount = min_order_amount;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public String getMorning_close_time() {
        return morning_close_time;
    }

    public void setMorning_close_time(String morning_close_time) {
        this.morning_close_time = morning_close_time;
    }

    public String getNoon_open_time() {
        return noon_open_time;
    }

    public void setNoon_open_time(String noon_open_time) {
        this.noon_open_time = noon_open_time;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVideo_path() {
        return video_path;
    }

    public void setVideo_path(String video_path) {
        this.video_path = video_path;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public boolean isIs_pure_veg() {
        return is_pure_veg;
    }

    public void setIs_pure_veg(boolean is_pure_veg) {
        this.is_pure_veg = is_pure_veg;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }
}
