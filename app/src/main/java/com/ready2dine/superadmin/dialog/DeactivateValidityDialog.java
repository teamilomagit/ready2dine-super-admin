package com.ready2dine.superadmin.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.util.numberpicker.NumberPicker;

/**
 * Created by Kalpana on 3/16/2018.
 */

public class DeactivateValidityDialog extends Dialog {

    Context context;

    NumberPicker npValidity;
    Button btnClose, btnDeactivate;
    DeactivateValidityDialogInterface listener;

    public DeactivateValidityDialog(@NonNull Context context, DeactivateValidityDialogInterface listener) {
        super(context);
        this.context = context;
        this.listener = listener;
    }

    public interface DeactivateValidityDialogInterface
    {
         void getDeactivateButtonClick();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_deactivate_validity);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        init();
    }

    private void init() {

        npValidity = findViewById(R.id.np_deactivate_validity);
        btnClose = findViewById(R.id.btn_close);
        btnDeactivate = findViewById(R.id.btn_deactivate);

        setUpCustomNumberPickers();
        setListeners();
    }


    private void setUpCustomNumberPickers() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "lato_regular.ttf");

        /*first picker*/
        npValidity.setMinValue(1);
        npValidity.setMaxValue(23);
        npValidity.setTypeface(font);

    }


    private void setListeners() {

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        btnDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.getDeactivateButtonClick();
                dismiss();
            }
        });
    }


}
