package com.ready2dine.superadmin.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.util.numberpicker.NumberPicker;

/**
 * Created by Kalpana on 3/16/2018.
 */

public class RestaurantTimeSetDialog extends Dialog {

    Context context;

    NumberPicker npHour;
    NumberPicker npMin;
    NumberPicker npPeriod;

    NumberPicker npHourClosed;
    NumberPicker npMinClosed;
    NumberPicker npPeriodClosed;

    ImageView imgClose, imgConfirm;
    SeekBar seekBar;
    LinearLayout llTopLayout;

    String[] period = {"AM", "PM"};


    public RestaurantTimeSetDialog(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_res_time_set);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
        init();
    }

    private void init() {

        npHour = findViewById(R.id.np_hour_open);
        npMin = findViewById(R.id.np_min_open);
        npPeriod = findViewById(R.id.np_period_open);

        npHourClosed = findViewById(R.id.np_hour_close);
        npMinClosed = findViewById(R.id.np_min_close);
        npPeriodClosed = findViewById(R.id.np_period_close);


        imgClose = findViewById(R.id.img_close);
        imgConfirm = findViewById(R.id.img_confirm);


        llTopLayout = findViewById(R.id.ll_top_layout);

        setUpCustomNumberPickers();
        setListeners();
    }


    private void setUpCustomNumberPickers() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "lato_regular.ttf");

        /*first picker*/
        npHour.setMinValue(1);
        npHour.setMaxValue(12);
        npHour.setTypeface(font);

        npMin.setMinValue(01);
        npMin.setMaxValue(59);
        npMin.setTypeface(font);
        npMin.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return String.format("%02d", value);
            }
        });
        npPeriod.setMinValue(0);
        npPeriod.setMaxValue(1);
        npPeriod.setDisplayedValues(period);
        npPeriod.setTypeface(font);


        /*second picker*/
        npHourClosed.setMinValue(1);
        npHourClosed.setMaxValue(12);
        npHourClosed.setTypeface(font);

        npMinClosed.setMinValue(01);
        npMinClosed.setMaxValue(59);
        npMinClosed.setTypeface(font);
        npMinClosed.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return String.format("%02d", value);
            }
        });
        npPeriodClosed.setMinValue(0);
        npPeriodClosed.setMaxValue(1);
        npPeriodClosed.setDisplayedValues(period);
        npPeriodClosed.setTypeface(font);

    }


    private void setListeners() {

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        imgConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        llTopLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }


}
