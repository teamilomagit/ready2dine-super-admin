package com.ready2dine.superadmin.activity;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.dialog.CustomAlertDialog;
import com.ready2dine.superadmin.fragment.CurrentOrdersFragment;
import com.ready2dine.superadmin.fragment.InProcessFragment;
import com.ready2dine.superadmin.fragment.InfoFragment;
import com.ready2dine.superadmin.fragment.ProfileFragment;
import com.ready2dine.superadmin.model.UserModel;
import com.ready2dine.superadmin.util.APIManager;
import com.ready2dine.superadmin.util.AppConstant;
import com.ready2dine.superadmin.util.AppUtils;
import com.ready2dine.superadmin.util.BaseActivity;
import com.ready2dine.superadmin.util.Config;
import com.ready2dine.superadmin.util.CustomLoader;
import com.ready2dine.superadmin.util.CustomTypefaceSpan;
import com.ready2dine.superadmin.util.NotificationUtils;
import com.ready2dine.superadmin.util.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

public class HomeActivity extends BaseActivity {
    Context context;
    BottomNavigationView mBottomNavigationView;
    private CustomLoader loader;
    private boolean doubleBackToExitPressedOnce = false;
    private ImageView imgLogOut;
    private TextView txtStaging;

    //FCM
    private static final String TAG = HomeActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();
    }


    /**
     * INITIALIZATION
     **/

    private void init() {
        context = this;
        imgLogOut = findViewById(R.id.img_log_out);
        loader = new CustomLoader(context);
        txtStaging = findViewById(R.id.txt_staging);

        setUpBottomNavigationView();
        setListeners();
        setUpBroadcastReceiverForFCM();
        updateDeviceToken();

        AppUtils.setPlaying(context, false);

        if (AppConstant.BASE_URL.contains("stag")) {
            txtStaging.setVisibility(View.VISIBLE);
        } else {
            txtStaging.setVisibility(View.GONE);
        }

    }

    /**
     * FCM
     **/
    private void setUpBroadcastReceiverForFCM() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getStringExtra("message");
//                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };
        displayFirebaseRegId();
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e(TAG, "Firebase reg id: " + regId);
    }

    private void setListeners() {
        imgLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupLogoutDialog();
            }
        });
    }


    private void setUpBottomNavigationView() {

        loadFragment(new CurrentOrdersFragment());

        mBottomNavigationView = findViewById(R.id.bottom_navigation_view);
        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;
                switch (item.getItemId()) {
                    case R.id.current_orders:
                        fragment = new CurrentOrdersFragment();
                        loadFragment(fragment);
                        return true;
                    case R.id.in_process:
                        fragment = new InProcessFragment();
                        loadFragment(fragment);
                        return true;
                    case R.id.info:
                        fragment = new InfoFragment();
                        loadFragment(fragment);
                        return true;
                }
                return false;
            }
        });
    }

    public void setBadgeCount(int count) {
        BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) mBottomNavigationView.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(0);
        BottomNavigationItemView itemView = (BottomNavigationItemView) v;

        View badge = LayoutInflater.from(this).inflate(R.layout.notification_badge, bottomNavigationMenuView, false);
        TextView notificationCount = badge.findViewById(R.id.notifications_badge);
        notificationCount.setText("" + count);

        itemView.addView(badge);
        setBottomNavigationViewFont();
    }

    private void setBottomNavigationViewFont() {
        Typeface font = Typeface.createFromAsset(getAssets(), "lato_light.ttf");
        CustomTypefaceSpan typefaceSpan = new CustomTypefaceSpan("", font);

        for (int i = 0; i < mBottomNavigationView.getMenu().size(); i++) {
            MenuItem menuItem = mBottomNavigationView.getMenu().getItem(i);
            SpannableStringBuilder spannableTitle = new SpannableStringBuilder(menuItem.getTitle());
            spannableTitle.setSpan(typefaceSpan, 0, spannableTitle.length(), 0);
            menuItem.setTitle(spannableTitle);
        }
    }


    public void removeBadge(BottomNavigationView navigationView, int index) {
        BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) navigationView.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(index);
        BottomNavigationItemView itemView = (BottomNavigationItemView) v;
        itemView.removeViewAt(itemView.getChildCount() - 1);
    }


    private void loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, fragment).commit();
        }
    }

    /**
     * CUSTOM DIALOGS
     **/
    private void setupLogoutDialog() {
        final CustomAlertDialog dialog = new CustomAlertDialog(context);
        dialog.show();

        dialog.txtMessage.setText(getString(R.string.sign_out_confirm));
        dialog.btnDeactivate.setText(getString(R.string.sign_out));

        dialog.btnDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutAPI();
                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    /**
     * HANDLER
     **/


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;

        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    /**
     * API CALLING
     **/

    private void updateDeviceToken() {
        UserModel userModel = null;
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);

        String token = pref.getString("regId", "");
        userModel = new SessionManager(context).getUserModel();

        if (userModel != null && !token.equalsIgnoreCase("")) {
            if (token.length() > 0) {
                JSONObject jsonObject = new JSONObject();
                try {

                    jsonObject.put("user_id", userModel.getId());
                    jsonObject.put("device_type", "ANDROID");
                    jsonObject.put("token", token);
                    jsonObject.put("app_type", "SUPER_ADMIN");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                new APIManager().PostAPI(AppConstant.API_SAVE_PUSH_TOKEN, jsonObject, null, context, new APIManager.APIManagerInterface() {
                    @Override
                    public void onSuccess(Object resultObj, String msg, Object data) {
//                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(String error) {
                        //Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }


    private void logoutAPI() {
        loader.showLoader();

        UserModel userModel = new SessionManager(context).getUserModel();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", userModel.getId());
            jsonObject.put("app_type", "ADMIN");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().PostAPI(AppConstant.API_LOGOUT, jsonObject, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg, Object data) {
                new SessionManager(context).logoutUser();
                startActivity(new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                ((Activity) context).finish();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                loader.dismissLoader();
            }
        });
    }


}
