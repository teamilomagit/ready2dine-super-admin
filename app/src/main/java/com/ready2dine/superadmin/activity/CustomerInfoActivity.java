package com.ready2dine.superadmin.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.model.CustomerModel;
import com.ready2dine.superadmin.model.UserModel;
import com.ready2dine.superadmin.util.APIManager;
import com.ready2dine.superadmin.util.AppConstant;
import com.ready2dine.superadmin.util.BaseActivity;
import com.ready2dine.superadmin.util.CustomLoader;

import org.json.JSONException;
import org.json.JSONObject;

public class CustomerInfoActivity extends BaseActivity {
    private Context context;
    private Button btnViewWallet, btnSearchCustomer;
    private EditText edtCustomerMobile;
    private ImageView imgSearch;
    private TextView txtCustomerName, txtCustomerNumber, txtCustomerEmail;

    private CustomerModel customerModel;
    private CustomLoader loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_info);

        init();
    }

    private void init() {
        context = this;

        btnViewWallet = findViewById(R.id.btn_reward_details);
        btnSearchCustomer = findViewById(R.id.btn_reward_details);

        edtCustomerMobile = findViewById(R.id.edt_customer_number);
        imgSearch = findViewById(R.id.img_search);

        txtCustomerEmail = findViewById(R.id.txt_customer_email);
        txtCustomerName = findViewById(R.id.txt_customer_name);
        txtCustomerNumber = findViewById(R.id.txt_customer_phone);

        loader = new CustomLoader(context);

        setListeners();
        setCustomToolBarWithTitle("Customer Wallet");

    }

    private void setListeners() {

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtCustomerMobile.getText().toString().length() < 10) {
                    Toast.makeText(context, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
                    return;
                }

                getUserDetailByNumber();
            }
        });

        btnViewWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (customerModel != null) {
                    startActivity(new Intent(context, RewardHistoryActivity.class).putExtra("customer_id", customerModel.getId()));
                }
            }
        });
    }


    /**
     * API CALLING
     **/

    private void getUserDetailByNumber() {
        loader.showLoader();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("mobile", edtCustomerMobile.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().PostAPI(AppConstant.API_GET_PROFILE_FROM_NUMBER, jsonObject, CustomerModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg, Object extra) {
                customerModel = (CustomerModel) resultObj;
                txtCustomerName.setText(customerModel.getDisplay_name());
                txtCustomerNumber.setText(customerModel.getMobile());
                txtCustomerEmail.setText(customerModel.getEmail());

                btnSearchCustomer.setVisibility(View.VISIBLE);
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                loader.dismissLoader();
            }

            @Override
            public void onError(String error) {
                txtCustomerName.setText("");
                txtCustomerNumber.setText("");
                txtCustomerEmail.setText("");
                btnSearchCustomer.setVisibility(View.GONE);
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                loader.dismissLoader();
            }
        });
    }
}
