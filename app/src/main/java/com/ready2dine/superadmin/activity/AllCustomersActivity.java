package com.ready2dine.superadmin.activity;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.TextView;

import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.adapter.CustomerAdapter;
import com.ready2dine.superadmin.model.CustomerModel;
import com.ready2dine.superadmin.util.APIManager;
import com.ready2dine.superadmin.util.AppConstant;
import com.ready2dine.superadmin.util.BaseActivity;

import java.util.ArrayList;

public class AllCustomersActivity extends BaseActivity {
    private Context context;
    private TextView txtTotalCustomers;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CustomerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_customers);

        init();
    }

    /**
     * INITIALIZATION
     **/

    private void init() {
        context = this;
        txtTotalCustomers = findViewById(R.id.txt_total_customers);


        setCustomToolBarWithTitle("Todays Sign Up's");
        setUpRecyclerView();
        getCustomerListAPI();
    }


    private void setUpRecyclerView() {
        mAdapter = new CustomerAdapter(context, new ArrayList<CustomerModel>());
        RecyclerView mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout = findViewById(R.id.swipe_refresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCustomerListAPI();
            }
        });
    }


    /**
     * API CALLING
     **/

    private void getCustomerListAPI() {
        mSwipeRefreshLayout.setRefreshing(true);

        new APIManager().postJSONArrayAPIWithExtra(AppConstant.API_GET_CUSTOMERS, null, CustomerModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg, Object data) {
                String totalCount = (String) data;
                txtTotalCustomers.setText("Total : " + totalCount);
                mAdapter.updateAdapter((ArrayList<CustomerModel>) resultObj);
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onError(String error) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

    }

}
