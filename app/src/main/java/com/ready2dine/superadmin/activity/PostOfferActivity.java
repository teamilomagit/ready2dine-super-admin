package com.ready2dine.superadmin.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.util.APIManager;
import com.ready2dine.superadmin.util.AppConstant;
import com.ready2dine.superadmin.util.BaseActivity;
import com.ready2dine.superadmin.util.CustomLoader;
import com.ready2dine.superadmin.util.PhotoSelector;
import com.ready2dine.superadmin.util.SingleUploadBroadcastReceiver;

import net.gotev.uploadservice.MultipartUploadRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class PostOfferActivity extends BaseActivity implements SingleUploadBroadcastReceiver.Delegate {
    private Context context;
    private Button btnSubmit;
    private ImageView imgOffer;
    private EditText edtOfferName, edtImageUrl;
    private PhotoSelector photoSelector;
    private Uri filePath;
    final SingleUploadBroadcastReceiver uploadReceiver = new SingleUploadBroadcastReceiver();
    private CustomLoader loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_offer);

        setCustomToolBarWithTitle("Create Offer Notification");
        init();
    }

    private void init() {
        context = this;
        btnSubmit = findViewById(R.id.btn_submit);
        edtOfferName = findViewById(R.id.edt_offer);
        edtImageUrl = findViewById(R.id.edt_image_url);
        imgOffer = findViewById(R.id.img_offer);
        photoSelector = new PhotoSelector(context);
        loader = new CustomLoader(context);

        setListeners();
    }

    private void setListeners() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtOfferName.getText().toString().length() > 0 && edtImageUrl.getText().toString().length() > 0) {
                    sendImageData();
                } else {
                    Toast.makeText(context, "Please check image url or offer name", Toast.LENGTH_SHORT).show();
                }
            }
        });

        imgOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (photoSelector.isPermissionGranted(context)) {
                    photoSelector.selectImage(imgOffer);
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PhotoSelector.SELECT_FILE)
                filePath = photoSelector.onSelectFromGalleryResult(intent);
            else if (requestCode == PhotoSelector.REQUEST_CAMERA)
                filePath = photoSelector.onCaptureImageResult();
        }

    }

    /**
     * PERMISSION
     **/

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PhotoSelector.PERMISSION_ALL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    photoSelector.selectImage(imgOffer);
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
//                    AppUtils.showPermissionDisclaimerDialog(context);
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    /**
     * API CALLING
     **/

    private void sendImageData() {
        loader.showLoader();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("title", edtOfferName.getText().toString());
            jsonObject.put("image_url", edtImageUrl.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().PostAPI(AppConstant.API_POST_IMAGE, jsonObject, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg, Object extra) {
                edtOfferName.setText("");
                edtImageUrl.setText("");
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                loader.dismissLoader();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                loader.dismissLoader();
            }
        });

    }

    private void uploadPicAPI() {
        try {
            String uploadId = UUID.randomUUID().toString();

            uploadReceiver.setDelegate(this);
            uploadReceiver.setUploadID(uploadId);

            String path = "";
            if (filePath != null) {
                path = photoSelector.getPath(filePath, context);
                try {
                    new MultipartUploadRequest(this, uploadId, "")
                            .addFileToUpload(path, "file_path") //Adding file
                            .setMaxRetries(2)
                            .startUpload();

//                                                .setNotificationConfig(new UploadNotificationConfig())

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                }
            } else {

            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * MULTIPART METHODS
     **/
    @Override
    public void onProgress(int progress) {

    }

    @Override
    public void onProgress(long uploadedBytes, long totalBytes) {

    }

    @Override
    public void onError(Exception exception) {

    }

    @Override
    public void onCompleted(int serverResponseCode, byte[] serverResponseBody) {

    }

    @Override
    public void onCancelled() {

    }

    /**
     * HNADLER METHODS
     **/
    @Override
    protected void onResume() {
        super.onResume();
        uploadReceiver.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        uploadReceiver.unregister(this);
    }

}
