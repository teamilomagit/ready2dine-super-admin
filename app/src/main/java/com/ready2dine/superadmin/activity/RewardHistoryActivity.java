package com.ready2dine.superadmin.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.adapter.RewardsAdapter;
import com.ready2dine.superadmin.model.RewardModel;
import com.ready2dine.superadmin.util.APIManager;
import com.ready2dine.superadmin.util.AppConstant;
import com.ready2dine.superadmin.util.BaseActivity;
import com.ready2dine.superadmin.util.CustomLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RewardHistoryActivity extends BaseActivity {
    private Context context;
    private EditText edtAmount;
    private ImageView imgAddAmount;
    private RewardsAdapter mAdapter;
    private RadioButton radioButtonGift, radioButtonExpired;
    private TextView txtTotalAmount;
    private String customerID;
    private ArrayList<RewardModel> list;
    float total = 0;

    private CustomLoader loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward_history);

        init();
    }

    private void init() {
        context = this;
        edtAmount = findViewById(R.id.edt_reward_amount);
        imgAddAmount = findViewById(R.id.img_add);

        radioButtonExpired = findViewById(R.id.radio_expired);
        radioButtonGift = findViewById(R.id.radio_gift);

        txtTotalAmount = findViewById(R.id.txt_total_amount);

        list = new ArrayList<>();
        loader = new CustomLoader(context);

        setUpRecyclerView();
        setListeners();
        setCustomToolBarWithTitle("Rewards History");
        setIntentData();
    }

    private void setIntentData() {
        customerID = getIntent().getStringExtra("customer_id");
        getAllRewardTransaction();
    }

    private void setListeners() {

        imgAddAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    confirmationDialog();
                }
            }
        });

    }

    private void setUpRecyclerView() {
        mAdapter = new RewardsAdapter(context, list);
        RecyclerView mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);
    }


    /**
     * FUNCTIONALITY
     **/

    private void calculateTotalAmount() {
        total = 0;

        for (RewardModel rewardModel : list) {
            total = total + Float.parseFloat(rewardModel.getAmount());
        }

        txtTotalAmount.setText("Total : " +getString(R.string.rs_symbol) + " " + total);
    }


    /**
     * DIALOG
     **/

    private void confirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (radioButtonExpired.isChecked()) {
            builder.setMessage("Are you sure you want to remove " + edtAmount.getText().toString() + "?");
        } else {
            builder.setMessage("Are you sure you want to add " + edtAmount.getText().toString() + "?");
        }

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                modifyReward();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.show();
    }

    private boolean isValid() {
        if (edtAmount.getText().toString().length() == 0) {
            Toast.makeText(context, "Please enter valid amount", Toast.LENGTH_SHORT).show();
            return false;
        }

        float amount = Float.parseFloat(edtAmount.getText().toString());

        if (amount == 0) {
            Toast.makeText(context, "Amount cannot be zero", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (amount > total && radioButtonExpired.isChecked()) {
            Toast.makeText(context, "Amount cannot exceed total amount", Toast.LENGTH_SHORT).show();
            return false;
        }


        return true;
    }


    /**
     * API CALLING
     **/

    private void getAllRewardTransaction() {
        loader.showLoader();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("customer_id", customerID);
            jsonObject.put("get_all_transactions", false);
            if (radioButtonExpired.isChecked()) {
                jsonObject.put("get_all_transactions", false);
            } else {
                jsonObject.put("get_all_transactions", false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().postJSONArrayAPI(AppConstant.API_MY_SAVINGS_LIST, jsonObject, RewardModel.class, context, new APIManager.APIManagerInterface() {

            @Override
            public void onSuccess(Object resultObj, String msg, Object extra) {
                list = (ArrayList<RewardModel>) resultObj;
                mAdapter.updateAdapter(list);
                calculateTotalAmount();
                loader.dismissLoader();

            }


            @Override
            public void onError(String error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                loader.dismissLoader();
            }
        });
    }


    private void modifyReward() {
        loader.showLoader();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("customer_id", customerID);
            if (radioButtonExpired.isChecked()) {
                jsonObject.put("type", "EXPIRED");
                jsonObject.put("amount", "-" + edtAmount.getText().toString());
            } else {
                jsonObject.put("type", "GIFT_BOX");
                jsonObject.put("amount", edtAmount.getText().toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().PostAPI(AppConstant.API_MODIFY_WALLET, jsonObject, null, context, new APIManager.APIManagerInterface() {

            @Override
            public void onSuccess(Object resultObj, String msg, Object extra) {
                loader.dismissLoader();
                getAllRewardTransaction();
            }


            @Override
            public void onError(String error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                loader.dismissLoader();
            }
        });
    }

}
