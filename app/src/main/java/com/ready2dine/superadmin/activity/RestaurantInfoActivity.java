package com.ready2dine.superadmin.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.dialog.RestaurantTimeSetDialog;

public class RestaurantInfoActivity extends AppCompatActivity {
    Context context;
    TextView txtTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_info);
        init();
    }


    /**
     * INITIALIZATION
     **/
    private void init() {
        context = this;
        txtTime = findViewById(R.id.txt_res_time);

        setListeners();
    }

    private void setListeners() {
        txtTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUpTimePicker();
            }
        });
    }


    private void setUpTimePicker() {
        RestaurantTimeSetDialog dialog = new RestaurantTimeSetDialog(context);
        dialog.show();
    }
}
