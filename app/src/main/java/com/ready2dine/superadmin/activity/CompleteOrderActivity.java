package com.ready2dine.superadmin.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.adapter.CurrentOrderedFoodListAdapter;
import com.ready2dine.superadmin.model.OrderItemModel;
import com.ready2dine.superadmin.model.OrderModel;
import com.ready2dine.superadmin.util.APIManager;
import com.ready2dine.superadmin.util.AppConstant;
import com.ready2dine.superadmin.util.AppUtils;
import com.ready2dine.superadmin.util.BaseActivity;
import com.ready2dine.superadmin.util.CustomLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CompleteOrderActivity extends BaseActivity {
    ArrayList<OrderItemModel> list;
    Button btnCompleted;
    Context context;
    CurrentOrderedFoodListAdapter mAdapter;
    OrderModel orderModel;
    CardView cardViewSpecialRequest;
    CustomLoader customLoader;
    RecyclerView mRecyclerView;
    RelativeLayout rlRestaurantDiscount;
    TextView txtOrderID, txtSpecialRequest, txtCustomerNear, txtCustomerName, txtCustomerMobile, txtTimeGuests, txtSubTotal, txtGSTTitle, txtGSTAmount, txtResDiscount, txtResDiscTitle, txtGrandTotal;
    TextView txtResName, txtResNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_order);
        init();
        setCustomToolBarWithTitle(getString(R.string.order_detail_page_title));
    }


    /**
     * INITIALIZATION
     **/
    private void init() {
        btnCompleted = findViewById(R.id.btn_completed);
        context = this;
        cardViewSpecialRequest = findViewById(R.id.card_view_special_request);
        customLoader = new CustomLoader(context);
        list = new ArrayList<>();
        rlRestaurantDiscount = findViewById(R.id.rl_res_discount);

        txtSpecialRequest = findViewById(R.id.txt_special_request);
        txtOrderID = findViewById(R.id.txt_order_id);
        txtCustomerName = findViewById(R.id.txt_customer_name);
        txtCustomerMobile = findViewById(R.id.txt_customer_phone);
        txtTimeGuests = findViewById(R.id.txt_time_guests);

        txtSubTotal = findViewById(R.id.txt_sub_total);
        txtGrandTotal = findViewById(R.id.txt_grand_total);
        txtGSTAmount = findViewById(R.id.txt_gst_amount);
        txtGSTTitle = findViewById(R.id.txt_gst_title);
        txtResDiscount = findViewById(R.id.txt_res_discount);
        txtResDiscTitle = findViewById(R.id.txt_res_discount_title);
        txtResName = findViewById(R.id.txt_restaurant_name);
        txtResNumber = findViewById(R.id.txt_restaurant_phone);

        setIntentData();
        setListeners();
    }

    private void setListeners() {
        btnCompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ifTimeValid()) {
                    completeOrderAPI();
                } else {
                    Toast.makeText(context, "You cannot complete order before booking time", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    private void setIntentData() {
        orderModel = new Gson().fromJson(getIntent().getStringExtra("process_order_model"), OrderModel.class);

        if (orderModel != null) {
            setDetails();
            setUpRecyclerView();
            getOrderDetailAPI();
            calculateBill();

            if (orderModel.getStatus().equalsIgnoreCase(context.getString(R.string.order_accepted))) {
                btnCompleted.setVisibility(View.VISIBLE);
            } else {
                btnCompleted.setVisibility(View.GONE);
            }
        }
    }

    private void setDetails() {
        txtOrderID.setText(orderModel.getId());
        txtSubTotal.setText(context.getString(R.string.rs_symbol) + " " + orderModel.getSubtotal());
        txtCustomerName.setText(orderModel.getCustomer_name());
        txtCustomerMobile.setText(orderModel.getMobile());
        txtResName.setText(orderModel.getRestaurant_name());
        txtResNumber.setText(orderModel.getRestaurant_mobile());

        if (orderModel.getNumber_of_guest() > 1) {
            txtTimeGuests.setText(AppUtils.convertDate(context, orderModel.getBooking_date()) + " for " + orderModel.getNumber_of_guest() + " Guests");
        } else {
            txtTimeGuests.setText(AppUtils.convertDate(context, orderModel.getBooking_date()) + " for " + orderModel.getNumber_of_guest() + " Guest");
        }

        /*if (!orderModel.getCustomer_near_restro()) {
            txtCustomerNear.setVisibility(View.GONE);
        } else {
            txtCustomerNear.setVisibility(View.VISIBLE);
        }*/

        if (orderModel.getSpecial_request().equalsIgnoreCase("")) {
            cardViewSpecialRequest.setVisibility(View.GONE);
        } else {
            cardViewSpecialRequest.setVisibility(View.VISIBLE);
            txtSpecialRequest.setText(orderModel.getSpecial_request());
        }
    }

    //Calculate grand total with respect to gst and discount
    private void calculateBill() {
        float resDiscount = 0;
        float subTotal = orderModel.getSubtotal();
        float gstValue = subTotal * (orderModel.getTax_gst_percent() / 100);


        if (orderModel.getRestro_offer_id() != null) {
            rlRestaurantDiscount.setVisibility(View.VISIBLE);
            resDiscount = subTotal * (orderModel.getRestro_offer_discount() / 100);
            txtResDiscTitle.setText(context.getString(R.string.res_discount) + "(" + (int) orderModel.getRestro_offer_discount() + "%)");
        } else if (orderModel.getRestaurant_discount() > 0) {
            rlRestaurantDiscount.setVisibility(View.VISIBLE);
            resDiscount = subTotal * (orderModel.getRestaurant_discount() / 100);
            txtResDiscTitle.setText(context.getString(R.string.res_discount) + "(" + (int) orderModel.getRestaurant_discount() + "%)");
        }

        float grandTotal = subTotal + gstValue - resDiscount;

        txtGSTTitle.setText(context.getString(R.string.gst_amount) + "(" + (int) orderModel.getTax_gst_percent() + "%)");
        txtSubTotal.setText(context.getString(R.string.rs_symbol) + " " + orderModel.getSubtotal());
        txtGSTAmount.setText(context.getString(R.string.rs_symbol) + " " + new DecimalFormat("##.##").format(gstValue));
        txtResDiscount.setText("-" + context.getString(R.string.rs_symbol) + " " + new DecimalFormat("##.##").format(resDiscount));

        txtGrandTotal.setText(context.getString(R.string.rs_symbol) + " " + grandTotal);
    }


    private void setUpRecyclerView() {
        mAdapter = new CurrentOrderedFoodListAdapter(context, list);

        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);
    }

    private boolean ifTimeValid() {
        try {
            Date bookingDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(orderModel.getBooking_date());
            Date currentDate = new Date();

            if (currentDate.after(bookingDate)) {
                return true;
            } else {
                return false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }


    /**
     * API CALLING
     **/

    private void getOrderDetailAPI() {
        JSONObject jsonObject = new JSONObject();
        customLoader.showLoader();
        try {
            jsonObject.put("order_id", orderModel.getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().postJSONArrayAPI(AppConstant.API_ORDER_DETAIL, jsonObject, OrderItemModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg, Object data) {
                list = (ArrayList<OrderItemModel>) resultObj;
                mAdapter.updateAdapter(list);
                customLoader.dismissLoader();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                customLoader.dismissLoader();
            }
        });
    }


    private void completeOrderAPI() {
        JSONObject jsonObject = new JSONObject();
        customLoader.showLoader();
        try {
            jsonObject.put("order_id", orderModel.getId());
            jsonObject.put("status", context.getString(R.string.order_completed).toUpperCase());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().PostAPI(AppConstant.CHANGE_ORDER_STATUS, jsonObject, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg, Object data) {
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                customLoader.dismissLoader();
            }
        });
    }
}
