package com.ready2dine.superadmin.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.model.UserModel;
import com.ready2dine.superadmin.util.APIManager;
import com.ready2dine.superadmin.util.AppConstant;
import com.ready2dine.superadmin.util.CustomLoader;
import com.ready2dine.superadmin.util.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import static com.ready2dine.superadmin.util.AppUtils.isValidEmail;
import static com.ready2dine.superadmin.util.AppUtils.isValidMobile;

public class LoginActivity extends AppCompatActivity {
    Button btnLogin;
    Context context;
    CustomLoader customLoader;
    EditText edtEmail, edtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();
    }


    private void init() {
        context = this;
        btnLogin = findViewById(R.id.btn_login);
        customLoader = new CustomLoader(context);

        edtEmail = findViewById(R.id.edt_email);
        edtPassword = findViewById(R.id.edt_password);

        setListeners();
    }

    private void setListeners() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //authenticate and get owner details
                if (isValid()) {
                    userLoginAPI();
                }
            }
        });
    }

    /**
     * FUNCTIONALITY
     **/

    private boolean isValid() {

        if (!isValidEmail(edtEmail.getText().toString())) {

            /*if (isValidMobile(edtEmail.getText().toString())) {
                return true;
            } else {
                Toast.makeText(this, "Please enter valid email or mobile number", Toast.LENGTH_LONG).show();
            }*/
            return true;
        }

        if (edtPassword.getText().toString().toString().length() == 0 && edtPassword.getText().toString().toString().isEmpty()) {
            Toast.makeText(this, "Please enter valid password", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    /**
     * API CALLING
     **/

    private void userLoginAPI() {

        customLoader.showLoader();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", edtEmail.getText().toString());
            jsonObject.put("password", edtPassword.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().PostAPI(AppConstant.API_LOGIN_SUPER, jsonObject, UserModel.class , context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg, Object data) {
                UserModel userModel = (UserModel) resultObj;
                new SessionManager(context).createLoginSession(userModel);

                startActivity(new Intent(context, HomeActivity.class));
                customLoader.dismissLoader();
                finishAffinity();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                customLoader.dismissLoader();
            }
        });
    }

}
