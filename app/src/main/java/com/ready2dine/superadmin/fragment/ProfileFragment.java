package com.ready2dine.superadmin.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.activity.LoginActivity;
import com.ready2dine.superadmin.activity.ResetPasswordActivity;
import com.ready2dine.superadmin.activity.RestaurantInfoActivity;
import com.ready2dine.superadmin.dialog.CustomAlertDialog;
import com.ready2dine.superadmin.dialog.DeactivateValidityDialog;
import com.ready2dine.superadmin.util.SessionManager;
import com.squareup.picasso.Picasso;


public class ProfileFragment extends Fragment {
    Context context;
    View view = null;
    TextView txtForRes, txtResetPwd, txtDeactivate, txtSignOut, txtDeactivateValidity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_profile, null);
            init();
        }
        return view;
    }


    /**
     * INITIALIZE
     **/
    private void init() {
        context = getActivity();
        txtForRes = view.findViewById(R.id.txt_for_res);
        txtResetPwd = view.findViewById(R.id.txt_reset_pwd);
        txtDeactivate = view.findViewById(R.id.txt_deactivate);
        txtSignOut = view.findViewById(R.id.txt_sign_out);
        txtDeactivateValidity = view.findViewById(R.id.txt_deactivate_validity);

        setUpToolbar();
        setListeners();
    }


    private void setUpToolbar() {
        TextView txtTitle = view.findViewById(R.id.txt_title);
        txtTitle.setText(context.getString(R.string.profile_page_title));
        ImageView imgBack = view.findViewById(R.id.img_back);
        imgBack.setVisibility(View.INVISIBLE);
    }

    private void setListeners() {
        txtForRes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, RestaurantInfoActivity.class));
            }
        });

        txtResetPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, ResetPasswordActivity.class));
            }
        });

        txtDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUpDeactivateDialog();
            }
        });

        txtSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupLogoutDialog();
            }
        });
    }


    private void setUpDeactivateDialog() {
        //Checking for current active status
        if (!txtDeactivate.getText().toString().equalsIgnoreCase(getString(R.string.re_activate))) {
            DeactivateValidityDialog dialog = new DeactivateValidityDialog(context, new DeactivateValidityDialog.DeactivateValidityDialogInterface() {
                @Override
                public void getDeactivateButtonClick() {
                    txtDeactivateValidity.setVisibility(View.VISIBLE);
                    txtDeactivate.setText(getString(R.string.re_activate));
                }
            });
            dialog.show();
        } else {
            txtDeactivate.setText(getString(R.string.deactivate));
            txtDeactivateValidity.setVisibility(View.GONE);
            Toast.makeText(context, "Activated account", Toast.LENGTH_SHORT).show();
        }
    }

    private void setupLogoutDialog() {
        CustomAlertDialog dialog = new CustomAlertDialog(context);
        dialog.show();

        dialog.txtMessage.setText(getString(R.string.sign_out_confirm));
        dialog.btnDeactivate.setText(getString(R.string.sign_out));

        dialog.btnDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SessionManager(context).logoutUser();
                startActivity(new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                ((Activity) context).finish();
            }
        });

    }


}
