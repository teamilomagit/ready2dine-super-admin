package com.ready2dine.superadmin.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.activity.HomeActivity;
import com.ready2dine.superadmin.adapter.CurrentOrderListAdapter;
import com.ready2dine.superadmin.model.OrderModel;
import com.ready2dine.superadmin.util.APIManager;
import com.ready2dine.superadmin.util.AppConstant;
import com.ready2dine.superadmin.util.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CurrentOrdersFragment extends Fragment {
    Context context;
    View view = null;
    RecyclerView mRecyclerView;
    CurrentOrderListAdapter mAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<OrderModel> list;
    TextView txtNoData, txtCurrentOrderCount;
    private BroadcastReceiver mReceiver;

    //Delay
    private Handler handler = null;
    private Runnable myRunnable;
    private boolean isFirstTime = true;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_current_orders, null);
            init();
        }
        return view;
    }

    /**
     * INITIALIZE
     **/
    private void init() {
        context = getActivity();
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh);
        list = new ArrayList<>();
        txtNoData = view.findViewById(R.id.txt_no_data);
        txtCurrentOrderCount = view.findViewById(R.id.txt_order_count);
        setUpRecyclerView();
        setUpToolbar();

    }


    private void callCurrentOrderAPIRepeatedly() {
        handler = new Handler();

        myRunnable = new Runnable() {
            @Override
            public void run() {
                //Get current orders from server
                getCurrentOrdersAPI();
                handler.postDelayed(myRunnable, 10000);
            }
        };

        handler.postDelayed(myRunnable, 10000);
    }

    private void setUpToolbar() {
        TextView txtTitle = view.findViewById(R.id.txt_title);
        txtTitle.setText(context.getString(R.string.current_orders_page_title));
        ImageView imgBack = view.findViewById(R.id.img_back);
        imgBack.setVisibility(View.INVISIBLE);
    }

    private void setUpRecyclerView() {
        mAdapter = new CurrentOrderListAdapter(context, list);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);

        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(context, R.color.colorPrimary), ContextCompat.getColor(context, R.color.colorPrimaryDark2));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCurrentOrdersAPI();
            }
        });
    }


    /**
     * API CALLING
     **/

    private void getCurrentOrdersAPI() {
        swipeRefreshLayout.setRefreshing(true);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "CURRENT");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        new APIManager().postJSONArrayAPI(AppConstant.API_GET_ORDER_SUPER, jsonObject, OrderModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg, Object data) {

                list = (ArrayList<OrderModel>) resultObj;
                if (list.size() > 0) {
                    txtNoData.setVisibility(View.GONE);
                } else {
                    txtNoData.setVisibility(View.VISIBLE);
                }
                mAdapter.updateAdapter(list);
                txtCurrentOrderCount.setText(list.size() + " " + context.getString(R.string.new_order_count));
                swipeRefreshLayout.setRefreshing(false);

                ((HomeActivity) getActivity()).setBadgeCount(list.size());
                /*Intent i = new Intent("CURRENT_ORDERS_COUNT").putExtra("badge_count", list.size());
                getActivity().getApplicationContext().sendBroadcast(i);*/

                if (isFirstTime) {
                    isFirstTime = false;
                    //Set post delay for repeated calling after delay
                    callCurrentOrderAPIRepeatedly();
                }
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        //Get current orders from server
        getCurrentOrdersAPI();

        IntentFilter intentFilter = new IntentFilter("MY_BROADCAST");
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    getCurrentOrdersAPI();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        context.registerReceiver(mReceiver, intentFilter);

    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            context.unregisterReceiver(mReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        try {
            context.unregisterReceiver(mReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (handler != null) {
            handler.removeCallbacks(myRunnable);
        }
        super.onDestroy();
    }
}
