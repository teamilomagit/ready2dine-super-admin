package com.ready2dine.superadmin.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.activity.HomeActivity;
import com.ready2dine.superadmin.adapter.OrderStatusListAdapter;
import com.ready2dine.superadmin.model.OrderModel;
import com.ready2dine.superadmin.util.APIManager;
import com.ready2dine.superadmin.util.AppConstant;
import com.ready2dine.superadmin.util.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class InProcessFragment extends Fragment {
    Context context;
    View view = null;
    RecyclerView mRecyclerView;
    OrderStatusListAdapter mAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<OrderModel> list;
    TextView txtNoData, txtInProcessOrderCount;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_in_process, null);
            init();
        }
        return view;
    }

    /**
     * INITIALIZE
     **/
    private void init() {
        context = getActivity();
        list = new ArrayList<>();
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh);
        txtNoData = view.findViewById(R.id.txt_no_data);
        txtInProcessOrderCount = view.findViewById(R.id.txt_order_count);
        setUpRecyclerView();
        setUpToolbar();
    }

    private void setUpToolbar() {
        TextView txtTitle = view.findViewById(R.id.txt_title);
        txtTitle.setText(context.getString(R.string.status_orders_page_title));
        ImageView imgBack = view.findViewById(R.id.img_back);
        imgBack.setVisibility(View.INVISIBLE);
    }


    private void setUpRecyclerView() {
        mAdapter = new OrderStatusListAdapter(context, list);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);

        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(context, R.color.colorPrimary), ContextCompat.getColor(context, R.color.colorPrimaryDark2));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getInProcessOrdersAPI();
            }
        });
    }


    /**
     * API CALLING
     **/

    private void getInProcessOrdersAPI() {

        swipeRefreshLayout.setRefreshing(true);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", "IN_PROCESS");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().postJSONArrayAPI(AppConstant.API_GET_ORDER_SUPER, jsonObject, OrderModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg, Object data) {

                list = (ArrayList<OrderModel>) resultObj;
                if (list.size() > 0) {
                    txtNoData.setVisibility(View.GONE);
                } else {
                    txtNoData.setVisibility(View.VISIBLE);
                }
                mAdapter.updateAdapter(list);
                swipeRefreshLayout.setRefreshing(false);
                txtInProcessOrderCount.setText(list.size() + " " + context.getString(R.string.new_order_count));
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        getInProcessOrdersAPI();
    }
}
