package com.ready2dine.superadmin.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ready2dine.superadmin.R;
import com.ready2dine.superadmin.activity.AllCustomersActivity;
import com.ready2dine.superadmin.activity.CustomerInfoActivity;
import com.ready2dine.superadmin.activity.PostOfferActivity;
import com.ready2dine.superadmin.model.UserModel;
import com.ready2dine.superadmin.util.SessionManager;

public class InfoFragment extends Fragment {
    private Context context;
    private TextView txtRestaurantName, txtEmail, txtAppVersion, txtCustomerInfo, txtCreateOffer, txtCustomerList;
    private View view;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_info, null);
            init();
        }
        return view;
    }


    private void init() {
        context = getActivity();
        txtAppVersion = view.findViewById(R.id.txt_version);
        txtEmail = view.findViewById(R.id.txt_email);
        txtRestaurantName = view.findViewById(R.id.txt_restaurant_name);
        txtCustomerInfo = view.findViewById(R.id.txt_customer_info);
        txtCreateOffer = view.findViewById(R.id.txt_customer_offer);
        txtCustomerList = view.findViewById(R.id.txt_customer_list);

        setData();

        txtCustomerInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, CustomerInfoActivity.class));
            }
        });

        txtCreateOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, PostOfferActivity.class));
            }
        });

        txtCustomerList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, AllCustomersActivity.class));
            }
        });
    }

    private void setData() {
        UserModel userModel = new SessionManager(context).getUserModel();

        if (userModel != null) {
            txtEmail.setText(userModel.getEmail());
            txtRestaurantName.setText(userModel.getName());
        }

        //get app version from package
        String version = getAppVersion();
        if (version != null) {
            txtAppVersion.setText(version);
        } else {
            txtAppVersion.setText("App version not available");
        }
    }

    private String getAppVersion() {
        String version = null;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return version;
    }
}
